﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tic_tac_toe
{
    public partial class Form1 : Form
    {
        Client client;
        public Form1()
        {
            InitializeComponent();
            client = new Client();
        }

        public void Form1_KeyDown(object sender, KeyEventArgs e)
        {
           
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            PictureBox pictureBox = sender as PictureBox;
            client.Compute(ref pictureBox);
        }

        private void UndoButton_Click(object sender, EventArgs e)
        {
            client.Undo();
        }

        private void RedoButton_Click(object sender, EventArgs e)
        {
            client.Redo();
        }
    }
}
