﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tic_tac_toe
{
    abstract class Command
    {
        abstract public void Execute();
        abstract public void UnExecute();
    }

    class TicTacToeConcrete : Command
    {
        private PictureBox pictureBox;
        private Reciever reciever;

        public TicTacToeConcrete(Reciever reciever, ref PictureBox pictureBox)
        {
            this.reciever = reciever;
            this.pictureBox = pictureBox;
        }

        public override void Execute()
        {
            reciever.Operation(ref pictureBox);
        }

        public override void UnExecute()
        {
            reciever.Operation(ref pictureBox);
        }
    }
    class Reciever
    {
        public void Operation(ref PictureBox pictureBox)
        {
            if (pictureBox.BackColor == Color.Red)
                pictureBox.BackColor = Color.White;
            else
                pictureBox.BackColor = Color.Red;
        }
    }

    class Client
    {
        private Reciever reciever = new Reciever();
        private List<Command> commands = new List<Command>();
        private int current = 0;

        public void Redo()
        {
            if (current <= commands.Count - 1)
            {
                Command command = commands[current];
                current++;
                command.Execute();
            }
        }

        public void Undo()
        {
            if (current > 0)
            {
                current--;
                Command command = commands[current];
                command.UnExecute();
            }
        }

        public void Compute(ref PictureBox pictureBox)
        {
            Command command = new TicTacToeConcrete(reciever, ref pictureBox);
            command.Execute();
            if (current < commands.Count - 1)
                commands.RemoveRange(current, commands.Count  - current);
            commands.Add(command);
            current++;
        }
    }
}
