﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facade
{
    class Program
    {
        class GPU
        {
            public void GPURun()
            {
                Console.WriteLine("GPU runned");
            }
            public void CheckConnecting()
            {
                Console.WriteLine("Connecting with monitor is checked");
            }
            public void DisplayRAMMemory()
            {
                Console.WriteLine("Ram info is displayed");
            }
            public void DisplayOpticDrive()
            {
                Console.WriteLine("Optic drive info is displayed");
            }
            public void DisplayHDD()
            {
                Console.WriteLine("HDD info is displayed");
            }
        }

        class RAM
        {
            public void RAMRun()
            {
                Console.WriteLine("RAM runned");
            }

            public void AnalyzeMemory()
            {
                Console.WriteLine("RAM memory analyzed");
            }
        }

        class HDD
        {
            public void HDDRun()
            {
                Console.WriteLine("HDD runned");
            }

            public void CheckBootSector()
            {
                Console.WriteLine("Boot sector is checked");
            }
        }

        class OpticDrive
        {
            public void OpticDriveRun()
            {
                Console.WriteLine("Optic drive runned");
            }

            public void CheckDisk()
            {
                Console.WriteLine("Checked disk availability");
            }
        }

        class PowerSupply
        {
            public void Run()
            {
                Console.WriteLine("Power energize");
            }

            public void EnergizeGPU()
            {
                Console.WriteLine("GPU energized");
            }

            public void EnergizeRAM()
            {
                Console.WriteLine("RAM energized");
            }

            public void EnergizeOpticDrive()
            {
                Console.WriteLine("Optic drive energized");
            }

            public void EnergizeHDD()
            {
                Console.WriteLine("HDD energized");
            }
        }

        class Sensors
        {
            public void CheckVoltage()
            {
                Console.WriteLine("Voltage checked");
            }

            public void CheckPowerSupplyTemperature()
            {
                Console.WriteLine("Power supply temperature is checked");
            }

            public void CheckGPUTemperature()
            {
                Console.WriteLine("GPU temperature is checked");
            }

            public void CheckRAMTemperature()
            {
                Console.WriteLine("RAM temperature is checked");
            }

            public void CheckAllSystemTemperature()
            {
                Console.WriteLine("All system temperature is checked");
            }
        }

        class Facade
        {
            GPU gpu = new GPU();
            RAM RAM = new RAM();
            HDD hdd = new HDD();
            OpticDrive opticDrive = new OpticDrive();
            PowerSupply powerSupply = new PowerSupply();
            Sensors sensors = new Sensors();

            public void BeginWork()
            {
                powerSupply.Run();
                sensors.CheckVoltage();
                sensors.CheckPowerSupplyTemperature();
                sensors.CheckGPUTemperature();
                powerSupply.EnergizeGPU();
                gpu.GPURun();
                sensors.CheckRAMTemperature();
                powerSupply.EnergizeRAM();
                RAM.RAMRun();
                RAM.AnalyzeMemory();
                gpu.DisplayRAMMemory();
                powerSupply.EnergizeOpticDrive();
                opticDrive.OpticDriveRun();
                opticDrive.CheckDisk();
                gpu.DisplayOpticDrive();
                powerSupply.EnergizeHDD();
                hdd.HDDRun();
                hdd.CheckBootSector();
                gpu.DisplayHDD();
                sensors.CheckAllSystemTemperature();
            }
        }

        static void Main(string[] args)
        {
            Facade facade = new Facade();
            facade.BeginWork();
        }
    }
}
