﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Flyweight
{
    public partial class Form2 : Form
    {
        ImageFactory imageFactory;
        Random random;
        public Form2()
        {
            imageFactory = new ImageFactory();
            random = new Random();
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            pictureBox1.Image = imageFactory.GetImage(random.Next(0, 4));
            pictureBox2.Image = imageFactory.GetImage(random.Next(0, 4));
            pictureBox3.Image = imageFactory.GetImage(random.Next(0, 4));
            pictureBox4.Image = imageFactory.GetImage(random.Next(0, 4));
        }
    }
}
