﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Flyweight
{
    abstract class MyImage
    {
        public Image image { get; set; }
        abstract public void LoadImage();
    }

    class Image1 : MyImage
    {
        public Image1()
        {
            LoadImage();
        }

        public override void LoadImage()
        {
            image = Image.FromFile(@"C: \Users\Dzaf_ow51\Downloads\smashicons.jpg");
        }
    }

    class Image2 : MyImage
    {
        public Image2()
        {
            LoadImage();
        }
        public override void LoadImage()
        {
            image = Image.FromFile(@"C: \Users\Dzaf_ow51\Downloads\unnamed.png");
        }
    }

    class Image3 : MyImage
    {
        public Image3()
        {
            LoadImage();
        }
        public override void LoadImage()
        {
            image = Image.FromFile(@"C: \Users\Dzaf_ow51\Downloads\home-icon.png");
        }
    }

    class Image4 : MyImage
    {
        public Image4()
        {
            LoadImage();
        }
        public override void LoadImage()
        {
            image = Image.FromFile(@"C: \Users\Dzaf_ow51\Downloads\BB.jpg");
        }
    }

    class Image5 : MyImage
    {
        public Image5()
        {
            LoadImage();
        }
        public override void LoadImage()
        {
            image = Image.FromFile(@"C: \Users\Dzaf_ow51\Downloads\GameOfThrones.jpg");
        }
    }

    class ImageFactory
    {
        private Dictionary<int, MyImage> images = new Dictionary<int, MyImage>();

        public Image GetImage(int index)
        {
            if (!images.ContainsKey(index))
            {
                switch (index)
                {
                    case 0:
                        images.Add(0, new Image1());
                        break;
                    case 1:
                        images.Add(1, new Image2());
                        break;
                    case 2:
                        images.Add(2, new Image3());
                        break;
                    case 3:
                        images.Add(3, new Image4());
                        break;
                    case 4:
                        images.Add(4, new Image5());
                        break;
                }
            }
            return images[index].image;
        }
    }
}
