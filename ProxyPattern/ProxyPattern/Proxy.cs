﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace ProxyPattern
{
    interface ITranslator
    {
        string Translate(string word);
    }

    class TranslatorProxy : ITranslator
    {
        ITranslator translator = new Translator();
        Dictionary<string, string> dictionary;

        public TranslatorProxy()
        {
            if (File.Exists("dictionary.bin"))
                using (Stream stream = File.Open("dictionary.bin", FileMode.Open))
                {
                    BinaryFormatter binaryFormatter = new BinaryFormatter();
                    dictionary = binaryFormatter.Deserialize(stream) as Dictionary<string, string>;
                }
            else
                dictionary = new Dictionary<string, string>();
        }

        public string Translate(string word)
        {
            if (!dictionary.ContainsKey(word))
                dictionary.Add(word, translator.Translate(word));
            return dictionary[word];
        }

        public void DictionarySerialize()
        {
            using (Stream stream = File.Open("dictionary.bin", FileMode.OpenOrCreate))
            {
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                binaryFormatter.Serialize(stream, dictionary);
            }
        }
    }

    class Translator : ITranslator
    {
        WebClient web;
        dynamic obj;
        public Translator()
        {
            web = new WebClient();
        }
        public string Translate(string word)
        {
            obj = JObject.Parse(web.DownloadString(@"https://translate.yandex.net/api/v1.5/tr.json/translate?lang=ru-en&key=trnsl.1.1.20180215T135739Z.f52558fd5495c3f6.004de4adb39c6f33e26ee4c17bee52a7dfb82045&text=" + word));
            string translatedWord  = Convert.ToString(obj.text);
            translatedWord =  translatedWord.Remove(0, 6);
            translatedWord =  translatedWord.Remove(translatedWord.Length - 4, 4);
            return translatedWord;
            // API key       trnsl.1.1.20180215T135739Z.f52558fd5495c3f6.004de4adb39c6f33e26ee4c17bee52a7dfb82045
        }
    }
}
