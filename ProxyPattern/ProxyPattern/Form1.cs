﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProxyPattern
{
    public partial class Form1 : Form
    {
        TranslatorProxy translatorProxy = null;
        public Form1()
        {
            InitializeComponent();
            translatorProxy = new TranslatorProxy();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void TranslateButton_Click(object sender, EventArgs e)
        {
            if (RusTextBox.Text != "")
                EngTextBox.Text =  translatorProxy.Translate(RusTextBox.Text);
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(translatorProxy != null)
                translatorProxy.DictionarySerialize();
        }
    }
}
