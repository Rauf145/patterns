﻿namespace ProxyPattern
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.RusLabel = new System.Windows.Forms.Label();
            this.EngLabel = new System.Windows.Forms.Label();
            this.RusTextBox = new System.Windows.Forms.TextBox();
            this.EngTextBox = new System.Windows.Forms.TextBox();
            this.TranslateButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // RusLabel
            // 
            this.RusLabel.AutoSize = true;
            this.RusLabel.Location = new System.Drawing.Point(107, 44);
            this.RusLabel.Name = "RusLabel";
            this.RusLabel.Size = new System.Drawing.Size(59, 17);
            this.RusLabel.TabIndex = 0;
            this.RusLabel.Text = "Russian";
            // 
            // EngLabel
            // 
            this.EngLabel.AutoSize = true;
            this.EngLabel.Location = new System.Drawing.Point(404, 44);
            this.EngLabel.Name = "EngLabel";
            this.EngLabel.Size = new System.Drawing.Size(54, 17);
            this.EngLabel.TabIndex = 1;
            this.EngLabel.Text = "English";
            // 
            // RusTextBox
            // 
            this.RusTextBox.Location = new System.Drawing.Point(65, 64);
            this.RusTextBox.Name = "RusTextBox";
            this.RusTextBox.Size = new System.Drawing.Size(151, 22);
            this.RusTextBox.TabIndex = 2;
            this.RusTextBox.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // EngTextBox
            // 
            this.EngTextBox.Location = new System.Drawing.Point(360, 64);
            this.EngTextBox.Name = "EngTextBox";
            this.EngTextBox.Size = new System.Drawing.Size(151, 22);
            this.EngTextBox.TabIndex = 3;
            // 
            // TranslateButton
            // 
            this.TranslateButton.Location = new System.Drawing.Point(249, 134);
            this.TranslateButton.Name = "TranslateButton";
            this.TranslateButton.Size = new System.Drawing.Size(86, 23);
            this.TranslateButton.TabIndex = 4;
            this.TranslateButton.Text = "Translate";
            this.TranslateButton.UseVisualStyleBackColor = true;
            this.TranslateButton.Click += new System.EventHandler(this.TranslateButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(588, 188);
            this.Controls.Add(this.TranslateButton);
            this.Controls.Add(this.EngTextBox);
            this.Controls.Add(this.RusTextBox);
            this.Controls.Add(this.EngLabel);
            this.Controls.Add(this.RusLabel);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label RusLabel;
        private System.Windows.Forms.Label EngLabel;
        private System.Windows.Forms.TextBox RusTextBox;
        private System.Windows.Forms.TextBox EngTextBox;
        private System.Windows.Forms.Button TranslateButton;
    }
}

