﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Composite
{
    abstract class Component
    {
        protected string name;
        public double Size { get; set; }
        public Component(string name, double size)
        {
            this.name = name;
            this.Size = size;
        }
        abstract public void Add(Component component);
        abstract public void Remove(Component component);
        abstract public void Display(int depth);
    }

    class File : Component
    {
        public File(string name, double size) : base(name, size) { }

        public override void Add(Component component) { }

        public override void Display(int depth) => Console.WriteLine(new String('-', depth) + name + " size:" + Size + " (File)");

        public override void Remove(Component component)
        {
            throw new NotImplementedException();
        }
    }

    class MyDirectory : Component
    {
        private List<Component> childrenDir = new List<Component>();

        public MyDirectory(string name, double size, string path) : base(name, size)
        {
            getDirInfo(this, path);
        }

        public override void Add(Component component) => childrenDir.Add(component);

        public override void Remove(Component component) => childrenDir.Remove(component);

        public override void Display(int depth)
        {
            Console.WriteLine(new String(' ', depth) + name + " size:" + Size + " (Folder)");
            foreach (var component in childrenDir)
            {
                component.Display(depth + 1);
            }
        }

        public void getDirInfo(MyDirectory dir, string path)
        {
            DirectoryInfo root = new DirectoryInfo(path);
            MyDirectory NewDir = null;
            double size = 0;
            foreach (var item in root.GetDirectories())
            {
                if (item.Exists)
                {
                    NewDir = new MyDirectory(item.Name, 0, item.FullName);
                    Add(NewDir);
                }
            }
            foreach (var fileName in root.GetFiles())
            {
                dir.Add(new File(fileName.Name, fileName.Length));
                size += fileName.Length;
            }
            if (NewDir != null)
                dir.Size += CountSize();
            else
                dir.Size = size;
        }

        private double CountSize()
        {
            double size = 0;
            foreach (var item in childrenDir)
            {
                size += item.Size;
            }
            return size;
        }
    }

    class Program
    {
        static int count = 0;
        static double size = 0;
        static void Main(string[] args)
        {
            MyDirectory root = new MyDirectory("Projects", 0, @"C:\Users\Rauf\Documents\Projects");
            root.Display(1);
        }
    }
}
