﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPattern
{
    class Car
    {
        public string CarName { get; set; }
        public string Engine { get; set; }
        public string Wheels { get; set; }
        public string Transmission { get; set; }
        public string Frame { get; set; }

        public override string ToString()
        {
            return $"{CarName}\n{Engine}\n{Wheels}\n{Transmission}\n{Frame}";
        }
    }
}
