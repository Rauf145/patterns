﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPattern
{
    class DaewooBuilder : CarBuilder
    {
        public DaewooBuilder()
        {
            car.CarName = "Lanos";
        }

        public override void CreateEngine()
        {
            car.Engine = "98 h/p";
        }

        public override void CreateFrame()
        {
            car.Frame = "Sedan";
        }

        public override void CreateTransmission()
        {
            car.Transmission = "5 Manual";
        }

        public override void CreateWheels()
        {
            car.Wheels = "13";
        }
    }
}
