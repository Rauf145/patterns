﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPattern
{
    class FordBuilder : CarBuilder
    {
        public FordBuilder()
        {
            car.CarName = "Probe";
        }

        public override void CreateEngine()
        {
            car.Engine = "160 h/p";
        }

        public override void CreateFrame()
        {
            car.Frame = "Coupe";
        }

        public override void CreateTransmission()
        {
            car.Transmission = "4 Auto";
        }

        public override void CreateWheels()
        {
            car.Wheels = "14";
        }

    }
}
