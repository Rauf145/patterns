﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPattern
{
    class UAZBuilder : CarBuilder
    {
        public UAZBuilder()
        {
            car.CarName = "Patriot";
        }

        public override void CreateEngine()
        {
            car.Engine = "120 h/p";
        }

        public override void CreateFrame()
        {
            car.Frame = "Universal";
        }

        public override void CreateTransmission()
        {
            car.Transmission = "4 Manual";
        }

        public override void CreateWheels()
        {
            car.Wheels = "16";
        }
    }
}
