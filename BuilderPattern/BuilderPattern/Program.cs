﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            Shop shop = new Shop();
            CarBuilder carBuilder = new HyundaiBuilder();
            shop.Construct(carBuilder);
            Console.WriteLine(carBuilder.car.ToString());
        }
    }
}
