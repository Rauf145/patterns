﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPattern
{
    class HyundaiBuilder: CarBuilder
    {
        public HyundaiBuilder()
        {
            car.CarName = "Getz";
        }

        public override void CreateEngine()
        {
            car.Engine = "66 h/p";
        }

        public override void CreateFrame()
        {
            car.Frame = "Хетчбэк";
        }

        public override void CreateTransmission()
        {
            car.Transmission = "4 auto";
        }

        public override void CreateWheels()
        {
            car.Wheels = "13";
        }
    }
}
