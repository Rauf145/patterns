﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPattern
{
    abstract class CarBuilder
    {
        public Car car = new Car();

        abstract public void CreateFrame();
        abstract public void CreateEngine();
        abstract public void CreateWheels();
        abstract public void CreateTransmission();
    }
}
