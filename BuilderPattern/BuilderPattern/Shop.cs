﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPattern
{
    class Shop
    {
        public void Construct(CarBuilder car)
        {
            car.CreateEngine();
            car.CreateFrame();
            car.CreateTransmission();
            car.CreateWheels();
        }
    }
}
