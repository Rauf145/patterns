﻿using System;

namespace BridgePattern
{
    interface IReport
    {
        void getInfo();
    }

    class CpuReporter : IReport
    {
        public void getInfo()
        {
            Console.WriteLine( "i7 7700hq\n2.8ghz\n4 threads");
        }
    }

    class GpuReporter : IReport
    {
        public void getInfo()
        {
            Console.WriteLine("Nvidia gtx 1050\n4gb videomemory");
        }
    }

    class HDDReporter : IReport
    {
        public void getInfo()
        {
            Console.WriteLine("Samsung\n512gb");
        }
    }

    class PcReporter
    {
        public IReport Report { get; set; }
        public void getInfo()
        {
            Report.getInfo();
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            PcReporter pcReporter = new PcReporter();
            pcReporter.Report = new HDDReporter();
            pcReporter.getInfo();
        }
    }
}
