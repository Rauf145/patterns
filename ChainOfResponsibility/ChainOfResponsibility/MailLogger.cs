﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsibility
{
    class MailLogger : Logger
    {
        public override void Logg(MyException ex)
        {
            StreamWriter streamWriter = new StreamWriter("log.txt");
            streamWriter.WriteLine(ex.Message);
            streamWriter.Dispose();
        }
    }
}
