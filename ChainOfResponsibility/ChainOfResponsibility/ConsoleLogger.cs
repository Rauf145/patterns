﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsibility
{
    class ConsoleLogger : Logger
    {
        public override void Logg(MyException ex)
        {
            Console.WriteLine(ex.Message);
            if(ex.errorLevel!= ErrorLevel.Low)
                Successor.Logg(ex);
        }
    }
}
