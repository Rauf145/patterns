﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsibility
{
    public enum ErrorLevel { Low, Normal, Critical}
    class MyException : Exception
    {
        public ErrorLevel errorLevel { get; set; }
        public MyException(string Message, ErrorLevel errorLevel) : base(Message)
        {
            this.errorLevel = errorLevel;
        }
    }
}
