﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsibility
{
    class FileLogger : Logger
    {
        public override void Logg(MyException ex)
        {
            Console.Beep(14000, 1000);
            if (ex.errorLevel != ErrorLevel.Normal)
                Successor.Logg(ex);
        }
    }
}
