﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ChainOfResponsibility
{
    class Program
    {
        static void Main(string[] args)
        {
            Logger concreate1 = new ConsoleLogger();
            Logger concreate2 = new FileLogger();
            Logger concreate3 = new MailLogger();
            concreate1.Successor = concreate2;
            concreate2.Successor = concreate3;
            try
            {
                throw new MyException("Error123", ErrorLevel.Critical);
            }
            catch (MyException ex)
            {
                concreate1.Logg(ex);
            }
        }
    }
}
