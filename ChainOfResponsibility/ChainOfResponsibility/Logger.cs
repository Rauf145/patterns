﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsibility
{
    abstract class Logger
    {
        public Logger Successor { get; set; }
        abstract public void Logg(MyException ex);
    }
}
