﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethod
{
    class S_Figure : Figure
    {
        public S_Figure()
        {
            Name = "  **\n**";
            arr = new int[4, 4];
            color.red = 24;
            color.green = 206;
            color.blue = 18;
            arr[1, 0] = 1;
            arr[1, 1] = 1;
            arr[0, 1] = 1;
            arr[0, 2] = 1;
        }
    }
}
