﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethod
{
    class J_Figure : Figure
    {
        public J_Figure()
        {
            Name = "***\n*";
            arr = new int[4,4];
            color.red = 205;
            color.green = 133;
            color.blue = 63;
            arr[0, 0] = 1;
            arr[0, 1] = 1;
            arr[0, 2] = 1;
            arr[1, 0] = 1;
        }
    }
}
