﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            Figure figure;
            FigureBuilder figureBuilder = new S_FigureBuilder();
            figure = figureBuilder.CreateFigure();
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    Console.SetCursorPosition(j, i);
                    if(figure.arr[i,j] == 1)
                    Console.WriteLine("*");
                }
            }
        }
    }
}
