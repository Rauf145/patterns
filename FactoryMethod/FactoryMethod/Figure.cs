﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethod
{
    struct FigureColor
    {
        public byte red;
        public byte green;
        public byte blue;
    }

    abstract class Figure
    {
        public int[,] arr;
        public string Name { get; set; }
        public FigureColor color;
    }
}
