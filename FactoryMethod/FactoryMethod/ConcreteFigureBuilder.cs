﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethod
{
    class T_FigureBuilder : FigureBuilder
    {
        public Figure CreateFigure()
        {
            return new T_Figure();
        }
    }

    class J_FigureBuilder : FigureBuilder
    {
        public Figure CreateFigure()
        {
            return new J_Figure();
        }
    }

    class L_FigureBuilder : FigureBuilder
    {
        public Figure CreateFigure()
        {
            return new L_Figure();
        }
    }

    class S_FigureBuilder : FigureBuilder
    {
        public Figure CreateFigure()
        {
            return new S_Figure();
        }
    }

    class Z_FigureBuilder : FigureBuilder
    {
        public Figure CreateFigure()
        {
            return new Z_Figure();
        }
    }
}
