﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethod
{
    class Z_Figure : Figure
    {
        public Z_Figure()
        {
            Name = "**\n  **";
            arr = new int[4, 4];
            color.red = 206;
            color.green = 16;
            color.blue = 98;
            arr[0, 0] = 1;
            arr[0, 1] = 1;
            arr[1, 1] = 1;
            arr[1, 2] = 1;
        }
    }
}
