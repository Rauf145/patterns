﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethod
{
    class L_Figure : Figure
    {
        public L_Figure()
        {
            Name = "*\n***";
            arr = new int[4, 4];
            color.red = 9;
            color.green = 136;
            color.blue = 239;
            arr[0, 0] = 1;
            arr[1, 0] = 1;
            arr[1, 1] = 1;
            arr[1, 2] = 1;
        }
    }
}
