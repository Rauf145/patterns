﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethod
{
    class T_Figure : Figure
    {
        public T_Figure()
        {
            Name = " *\n***";
            arr = new int[4, 4];
            color.red = 238;
            color.green = 244;
            color.blue = 66;
            arr[0, 1] = 1;
            arr[1, 1] = 1;
            arr[1, 0] = 1;
            arr[1, 2] = 1;
        }
    }
}
