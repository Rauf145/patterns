﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Memento
{
    class Caretaker
    {
        public List<Memento> mementes = new List<Memento>();
        int Current { get; set; }

        public void Add(Memento memento)
        {
            if (Check(memento))
            {
                if (Current + 1 <= mementes.Count - 1)
                    mementes.RemoveRange(Current + 1, mementes.Count - Current - 1);
                mementes.Add(memento);
                Current++;
            }
        }

        public Caretaker()
        {
            Current = -1;
        }

        public Memento this[int index]
        {
            get
            {
                if (index < mementes.Count)
                {
                    Current = index;
                    return mementes[index];
                }
                return null;
            }
        }

        public Memento Undo()
        {
            if (Current > 0 && Current < mementes.Count)
            {
                Current--;
                return mementes[Current];
            }
            return null;
        }

        public Memento Redo()
        {
            if (Current >= 0 && Current < mementes.Count - 1)
            {
                Current++;
                return mementes[Current];
            }
            return null;
        }

        public void Restore(ref ComboBox comboBox)
        {
            comboBox.Items.Clear();
            for (int i = 0; i < mementes.Count; i++)
                comboBox.Items.Add(mementes[i]);
            comboBox.DisplayMember = "Text";
        }

        public bool Check(Memento memento)
        {
            for (int i = 0; i < mementes.Count; i++)
                if(Current >= i && memento.Text == mementes[i].Text)
                    return false;
            return true;
        }
    }
}
