﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Memento
{
    public partial class Form1 : Form
    {
        Caretaker caretaker;

        public Form1()
        {
            InitializeComponent();
            caretaker = new Caretaker();
            Memento memento = new Memento("", richTextBox1.SelectionStart, richTextBox1.Font.Style, richTextBox1.SelectionLength);
            caretaker.Add(memento);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Memento memento = new Memento(richTextBox1.Text, richTextBox1.SelectionStart, richTextBox1.Font.Style, richTextBox1.SelectionLength);
            caretaker.Add(memento);
            caretaker.Restore(ref comboBox1);
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadMemento(caretaker[comboBox1.SelectedIndex]);
        }

        private void richTextBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Modifiers == Keys.Control && e.KeyCode == Keys.L)
                LoadMemento(caretaker[comboBox1.SelectedIndex]);
            else if (e.Modifiers == Keys.Control && e.KeyCode == Keys.S)
            {
                Memento memento = new Memento(richTextBox1.Text, richTextBox1.SelectionStart, richTextBox1.Font.Style, richTextBox1.SelectionLength);
                caretaker.Add(memento);
                caretaker.Restore(ref comboBox1);
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {
            FontStyle fontStyle = richTextBox1.SelectionFont.Style;

            if (richTextBox1.SelectionFont.Style != FontStyle.Bold)
                fontStyle |= FontStyle.Bold;
            else
                fontStyle &= ~FontStyle.Bold;
            richTextBox1.SelectionFont = new Font(richTextBox1.SelectionFont, fontStyle);
        }

        private void UndoRedo_Click(object sender, EventArgs e)
        {
            Memento memento = null;
            switch (((Button)sender).Text)
            {
                case "Undo":
                    memento = caretaker.Undo();
                    break;
                case "Redo":
                    memento = caretaker.Redo();
                    break;
                default:
                    break;
            }
            LoadMemento(memento);
        }

        private void LoadMemento(Memento memento)
        {
            if (memento != null)
            {
                FontStyle fontStyle = memento.fontStyle;
                richTextBox1.Text = memento.Text;
                richTextBox1.SelectionFont = new Font(richTextBox1.SelectionFont, fontStyle);
                richTextBox1.SelectionStart = memento.SelectionStart;
                richTextBox1.SelectionLength = memento.SelectedTextLength;
                richTextBox1.Focus();
            }
        }
    }
}
