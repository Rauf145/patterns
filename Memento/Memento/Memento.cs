﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Memento
{
    class Memento
    {
        public string Text { get; set; }
        public int SelectionStart { get; set; }
        public FontStyle fontStyle { get; set; }
        public int SelectedTextLength { get; set; }

        public Memento(string text,  int selectionStart, FontStyle fontStyle, int selectedTextLength)
        {
            Text = text;
            SelectedTextLength = selectedTextLength;
            this.fontStyle = fontStyle;
            SelectionStart = selectionStart;
        }
    }
}
