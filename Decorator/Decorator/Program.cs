﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator
{
    abstract class Character
    {
        public string Name { get; set; }
        private int _attack;
        private int _speed;
        private int _life;
        private int _defense;
        public virtual int Attack { get => _attack; set => _attack = value; }
        public virtual int Speed { get => _speed; set => _speed = value; }
        public virtual int Defense { get => _defense; set => _defense = value; }
        public virtual int Life { get => _life; set => _life = value; }
        public virtual void ShowStats()
        {
            Console.WriteLine($"{Name}, attact: {Attack}, speed: {Speed}, life: {Life}, defense: {Defense}");
        }
    }

    class Human : Character
    {
        public Human()
        {
            Name = "Human";
            Attack = 20;
            Speed = 20;
            Life = 150;
            Defense = 0;
        }
    }

    abstract class Decorator : Character
    {
        protected Character character;
        private int _attack;
        private int _speed;
        private int _life;
        private int _defense;
        public override int Defense { get => _defense + character.Defense; set => _defense = value; }
        public override int Life { get => _life + character.Life; set => _life = value; }
        public override int Speed { get => _speed + character.Speed; set => _speed = value; }
        public override int Attack { get => _attack + character.Attack; set => _attack = value; }
    }

    class HumanWarrior : Decorator
    {
        public HumanWarrior(Character character)
        {
            if (character is Human)
            {
                this.character = character;
                Name = "HumanWarrior";
                Attack = 20;
                Speed = 10;
                Life = 50;
                Defense = 20;
            }
            else
                throw new Exception("Not correct");
        }
    }

    class SwordMan : Decorator
    {
        public SwordMan(Character character)
        {
            if (character is HumanWarrior)
            {
                this.character = character;
                Name = "SwordMan";
                Attack = 40;
                Speed = -10;
                Life = 50;
                Defense = 40;
            }
            else
                throw new Exception("Not correct");
        }
    }

    class Archer : Decorator
    {
        public Archer(Character character)
        {
            if (character is HumanWarrior)
            {
                this.character = character;
                Name = "Archer";
                Attack = 20;
                Speed = 20;
                Life = 50;
                Defense = 10;
            }
            else
                throw new Exception("Not correct");
        }
    }

    class HorserMan : Decorator
    {
        public HorserMan(Character character)
        {
            if (character is SwordMan)
            {
                this.character = character;
                Name = "HorseMan";
                Attack = -10;
                Speed = 40;
                Life = 200;
                Defense = 100;
            }
            else
                throw new Exception("Not correct");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Character character = new Human();
                character.ShowStats();
                Decorator decorator = new HumanWarrior(character);
                decorator.ShowStats();
                Decorator swordMan = new SwordMan(decorator);
                swordMan.ShowStats();
                Decorator horseMan = new HorserMan(swordMan);
                horseMan.ShowStats();
                Decorator archer = new Archer(new HumanWarrior(new Human()));
                archer.ShowStats();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
